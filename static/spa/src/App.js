import React, { useEffect, useState, Fragment } from 'react';
// import { invoke } from '@forge/bridge';
import * as _ from 'lodash';

// Atlaskit
import { LoadingButton as Button } from '@atlaskit/button';
import { Checkbox } from '@atlaskit/checkbox';
import CloseIcon from '@atlaskit/icon/glyph/editor/close';
import TrashIcon from '@atlaskit/icon/glyph/editor/remove';
import Textfield from '@atlaskit/textfield';
import Lozenge from '@atlaskit/lozenge';
import Spinner from '@atlaskit/spinner';

// Custom Styles
import {
  Card, Row, Icon, IconContainer, Status, SummaryActions, SummaryCount, SummaryFooter,
  ScrollContainer, Form, LoadingContainer
} from './Styles';

function App() {
  const [todos, setTodos] = useState(null);
  const [input, setInput] = useState('');
  const [isFetched, setIsFetched] = useState(false);
  const [isDeleteAllShowing, setDeleteAllShowing] = useState(false);
  const [isDeletingAll, setDeletingAll] = useState(false);
  const [devTools, setDevTools] = useState(false);
  const [contextData, setContextData] = useState('');

  if (!isFetched) {
    setIsFetched(true);
      if (process.env.NODE_ENV !== 'development') {
          const {invoke} = require('@forge/bridge');
          invoke('get-all').then(setTodos);
      } else {
          setTodos([]);
      }
  }

  const safeGetContext = async (override) => {
      if (process.env.NODE_ENV === 'development') {
          const fakeIssueContext = {
              "localId": "ari:cloud:ecosystem::extension/....",
              "cloudId": ".....",
              "moduleKey": "....-edition-issue-panel",
              "extension": {
                  "issue": {
                      "key": "X-1",
                      "id": "10015",
                      "type": "Epic"
                  },
                  "project": {
                      "id": "10003",
                      "key": "X"
                  },
                  "isNewToIssue": false,
                  "type": "jira:issuePanel"
              },
              "accountId": "...."
          }
          return _.merge(fakeIssueContext, override);
      } else {
          const { view } = require('@forge/bridge');
          const context = await view.getContext();
          console.log('[production] context=',context);
          return context;
      }
  }

    const fetchContextData = async () => {
        const context = await safeGetContext({extension: {project:{key:'Override localfake data'}, issue:{key:'OLFD-1'}}})
        const title = `${context.extension.project.key} / ${context.extension.issue.key}`;
        console.log('showContextData', title, context);
        setContextData(title);
    }
  useEffect(() => {
      fetchContextData();
  })

  useEffect(() => {
      if (!devTools) {
          if (!window.__REDUX_DEVTOOLS_EXTENSION__) {
              console.log('DevTools not available :(');
              return false;
          }
          console.log('DevTools available!');
          const config = {};
          const devTools = window.__REDUX_DEVTOOLS_EXTENSION__.connect(config);
          setDevTools(devTools);
      }
  }, []);

  const createTodo = async (label) => {
    const newTodoList = [...todos, { label, isChecked: false, isSaving: true }];

    setTodos(newTodoList);
  }

  const toggleTodo = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return { ...todo, isChecked: !todo.isChecked, isSaving: true };
        }
        return todo;
      })
    )
  }

  const deleteTodo = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return { ...todo, isDeleting: true };
        }
        return todo;
      })
    )
  }

  const deleteAllTodos = async () => {
    setDeletingAll(true);
      if (process.env.NODE_ENV !== 'development') {
          const {invoke} = require('@forge/bridge');
          await invoke('delete-all');
      } else {
          // TODO: simulate action locally
      }


    setTodos([]);
    setDeleteAllShowing(false);
    setDeletingAll(false);
  }

  const onSubmit = (e) => {
    e.preventDefault();
    createTodo(input);
    setInput('');
  };

  const wrappedInvoke = (key, payload) => {
      devTools && devTools.send(key, payload);
      if (process.env.NODE_ENV !== 'development') {
          const {invoke} = require('@forge/bridge');
          return invoke(key, payload);
      } else {
          return payload;
      }
  }


  useEffect(() => {
    if (!todos) return;
    if (!todos.find(todo => todo.isSaving || todo.isDeleting)) return;

    Promise.all(
      todos.map((todo) => {
        if (todo.isSaving && !todo.id) {
          return wrappedInvoke('create', { label: todo.label, isChecked: false })
        }
        if (todo.isSaving && todo.id) {
          return wrappedInvoke('update', { id: todo.id, label: todo.label, isChecked: todo.isChecked })
        }
        if (todo.isDeleting && todo.id) {
          return wrappedInvoke('delete', { id: todo.id }).then(() => false);
        }
        return todo;
      })
    )
    .then(saved => saved.filter(a => a))
    .then(setTodos)
  }, [todos]);

  if (!todos) {
    return (
      <Card>
        <LoadingContainer>
          <Spinner size="large" />
        </LoadingContainer>
      </Card>
    );
  }

  const completedCount = todos.filter(todo => todo.isChecked).length;
  const totalCount = todos.length;

  const Rows = () => (
    <Fragment>
      {todos.map(({ id, label, isChecked, isSaving, isDeleting }, i) => {
        const isSpinnerShowing = isSaving || isDeleting;

        return (
          <Row isChecked={isChecked} key={label}>
            <Checkbox isChecked={isChecked} label={label} name={label} onChange={() => toggleTodo(id)} />
            <Status>
              {isSpinnerShowing ? <Spinner size="medium" /> : null}
              {isChecked ? <Lozenge appearance="success">Done</Lozenge> : null}
              <Button size="small" spacing="none" onClick={() => deleteTodo(id)}>
                <IconContainer>
                  <Icon>
                    <CloseIcon />
                  </Icon>
                </IconContainer>
              </Button>
            </Status>
          </Row>
        );
      })}
    </Fragment>
  );

  const DeleteAll = () => isDeleteAllShowing ? (
    <Button
      appearance="danger"
      spacing="compact"
      isLoading={isDeletingAll}
      isDisabled={isDeletingAll}
      onClick={deleteAllTodos}
    >
      Delete All
    </Button>
  ) : (
    <Button appearance="subtle" spacing="none" onClick={() => setDeleteAllShowing(true)}>
      <IconContainer>
        <Icon>
          <TrashIcon />
        </Icon>
      </IconContainer>
    </Button>
  );

  const CompletedLozenge = () => <Lozenge>{completedCount}/{totalCount} Completed</Lozenge>;

  return (
    <Card>
        <h2>{contextData}</h2>
      <ScrollContainer>
        <Rows />
        <Row isCompact>
          <Form onSubmit={onSubmit}>
            <Textfield
              appearance="subtle"
              placeholder="Add a todo +"
              value={input}
              onChange={({ target }) => setInput(target.value)}
            />
          </Form>
        </Row>
      </ScrollContainer>
      <SummaryFooter>
        <SummaryCount>
          <CompletedLozenge />
        </SummaryCount>
        <SummaryActions>
          <DeleteAll />
        </SummaryActions>
      </SummaryFooter>
    </Card>
  );
}

export default App;
